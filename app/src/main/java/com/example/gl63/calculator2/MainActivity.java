package com.example.gl63.calculator2;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private Button button1;
    private Button button2;
    private Button button3;
    private Button button4;
    private Button button5;
    private Button button6;
    private Button button7;
    private Button button8;
    private Button button9;
    private Button button0;
    private Button buttonAdd;
    private Button buttonSub;
    private Button buttonMul;
    private Button buttonDiv;
    private Button buttonClear;
    private Button buttonEqual;
    String result;
    String tmp;
    String operator;
    TextView resultTextView;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initWidget();
    }

    public void initWidget(){
        button1 = (Button) findViewById(R.id.button1);
        button1.setOnClickListener(this);

        button2 = (Button) findViewById(R.id.button2);
        button2.setOnClickListener(this);

        button3 = (Button) findViewById(R.id.button3);
        button3.setOnClickListener(this);

        button4 = (Button) findViewById(R.id.button4);
        button4.setOnClickListener(this);

        button5 = (Button) findViewById(R.id.button5);
        button5.setOnClickListener(this);

        button6 = (Button) findViewById(R.id.button6);
        button6.setOnClickListener(this);

        button7 = (Button) findViewById(R.id.button7);
        button7.setOnClickListener(this);

        button8 = (Button) findViewById(R.id.button8);
        button8.setOnClickListener(this);

        button9 = (Button) findViewById(R.id.button9);
        button9.setOnClickListener(this);

        button0 = (Button) findViewById(R.id.button0);
        button0.setOnClickListener(this);

        buttonAdd = (Button) findViewById(R.id.buttonAdd);
        buttonAdd.setOnClickListener(this);

        buttonSub = (Button) findViewById(R.id.buttonSub);
        buttonSub.setOnClickListener(this);

        buttonMul = (Button) findViewById(R.id.buttonMul);
        buttonMul.setOnClickListener(this);

        buttonDiv = (Button) findViewById(R.id.buttonDiv);
        buttonDiv.setOnClickListener(this);

        buttonClear = (Button) findViewById(R.id.buttonClear);
        buttonClear.setOnClickListener(this);

        buttonEqual = (Button) findViewById(R.id.buttonEqual);
        buttonEqual.setOnClickListener(this);

        resultTextView = (TextView)findViewById(R.id.textViewResult);

    }

    @Override
    public void onClick(View v){
        switch (v.getId()){
            case R.id.button1:
                //B1
                onNumberButtonClicked("1");
                break;

            case R.id.button2:
                //B2
                onNumberButtonClicked("2");
                break;

            case R.id.button3:
                //B3
                onNumberButtonClicked("3");
                break;

            case R.id.button4:
                //B4
                onNumberButtonClicked("4");
                break;

            case R.id.button5:
                //B5
                onNumberButtonClicked("5");
                break;

            case R.id.button6:
                //B6
                onNumberButtonClicked("6");
                break;

            case R.id.button7:
                //B7
                onNumberButtonClicked("7");
                break;

            case R.id.button8:
                //B8
                onNumberButtonClicked("8");
                break;

            case R.id.button9:
                //B9
                onNumberButtonClicked("9");
                break;

            case R.id.button0:
                //B0
                onNumberButtonClicked("0");
                break;

            case R.id.buttonAdd:
                //BAdd
                onOperatorButtonClicked("+");
                break;

            case R.id.buttonSub:
                //BSub
                onOperatorButtonClicked("-");
                break;

            case R.id.buttonMul:
                //BMul
                onOperatorButtonClicked("*");
                break;

            case R.id.buttonDiv:
                //BDiv
                onOperatorButtonClicked("/");
                break;

            case R.id.buttonClear:
                //BClear
                onClearButtonClicked();
                break;

            case R.id.buttonEqual:
                //BEqual
                onEqualButtonClicked();
                break;

        }
    }
    private void onNumberButtonClicked(String num){
        result = resultTextView.getText().toString();
        result = result + num;
        resultTextView.setText(result);
    }

    private void onOperatorButtonClicked(String operator){
        tmp = resultTextView.getText().toString();
        onClearButtonClicked();
        this.operator = operator;
    }

    private void onClearButtonClicked(){
        result = "";
        resultTextView.setText("");
    }

    private void onEqualButtonClicked(){
        int res = 0;
        try {
            int number = Integer.valueOf(tmp);
            int number2 = Integer.valueOf(resultTextView.getText().toString());
            switch (operator) {
                case "+":
                    res = number + number2;
                    break;
                case "/":
                    res = number / number2;
                    break;
                case "-":
                    res = number - number2;
                    break;
                case "*":
                    res = number * number2;
                    break;
            }
            result = String.valueOf(res);
            resultTextView.setText(result);
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }

}
